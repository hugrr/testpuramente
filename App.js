import React, { useEffect, useState } from "react";
import { SafeAreaView, StatusBar, View } from "react-native";
import moment from "moment";
const axios = require("axios");
import {
  NativeBaseProvider,
  Image,
  Center,
  Button,
  HStack,
  Text,
  VStack,
} from "native-base";

const App: () => Node = () => {
  const [count, setCount] = useState([]);
  const [racha, setRacha] = useState([]);

  async function getUserData() {
    try {
      const response = await axios.get(
        "https://gist.githubusercontent.com/nahuelb/0af04ce9aadab10afe2f37ba566070c2/raw/47effc9a678e9616369b56eeeb4ee54f22763b21/sessions.json"
      );
      //filtrando solo las sesiones terminadas
      const result = response.data.filter(
        (item) => item.isSessionCompleted === true
      );
      let array = [];
      // agregando al array la fecha formateada
      result.map((item, index) => {
        if (array.includes(moment(item.dateSession).format("YYYY-MM-DD"))) {
        } else {
          array.push(moment(item.dateSession).format("YYYY-MM-DD"));
        }
      });
      // convirtiendo el arreglo de fecha  en un arreglo de numeros
      const ArrnNum = array.map((item, index) => {
        const data = item.split("-");
        return data;
      });
      // definiendovariables para recorrer el arreglo y calcular
      let count = 0;
      let racha = [];
      for (let i = 0; i < ArrnNum.length; i++) {
        let A = parseInt(ArrnNum[i][2]);
        let B = parseInt(ArrnNum[i + 1][2]);
        //defieniendo dias de los meses
        mes = new Date(
          parseInt(ArrnNum[i][0]),
          parseInt(ArrnNum[i][1]),
          0
        ).getDate();
        //sumando el numero consecutivo y validando que sume alempezar el mes asi obtengo la racha actual
        if (A + 1 === B || mes / B === mes) {
          count++;
          console.log(count);
        } else count = 0;
        racha.push(count);
        setCount(count + 1);
        //obteniendo numero  mas alto del arreglo de numeros por consecuente la racha maxima alcanzada
        var max = Math.max(...racha);
        setRacha(max);
      }
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {}, []);

  useEffect(() => {
    getUserData();
  }, []);

  return (
    <NativeBaseProvider>
      <SafeAreaView>
        <StatusBar barStyle={"dark-content"} />
        <Center>
          <View>
            <Text color={"grey"} fontSize="5xl">
              pura mente
            </Text>
          </View>
          <View
            style={{
              paddingBottom: 60,
            }}
          >
            <Center>
              <Image
                source={{
                  uri: "https://puramente.app/wp-content/uploads/2021/01/Ilustraciones-horarios-02-1-1.png",
                }}
                alt="Alternate Text"
                size="2xl"
              />
            </Center>
          </View>
          <View>
            <HStack>
              <VStack>
                <Center>
                  <Text paddingRight={20} color={"grey"} fontSize="lg">
                    Racha máxima
                  </Text>
                  <Text paddingRight={20} color={"grey"} fontSize="4xl">
                    {racha}
                  </Text>
                </Center>
              </VStack>
              <VStack>
                <Center>
                  <Text color={"grey"} fontSize="lg">
                    Días racha actual
                  </Text>
                  <Text color={"grey"} fontSize="4xl">
                    {count}
                  </Text>
                </Center>
              </VStack>
            </HStack>
          </View>
        </Center>
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default App;
